<?php

class BridgeTestimonialModule extends FLBuilderModule {

	public function __construct()
	{
		parent::__construct(array(
			'name'              => __( 'Testimonial', 'bb-bridge' ),
			'description'       => __( 'Addon to display your testimonials.', 'bb-bridge' ),
			'category'          => __( 'Bridge Modules', 'bb-bridge' ),
			'dir'               => BB_BRIDGE_DIR . 'modules/bridge-testimonial/',
			'url'               => BB_BRIDGE_URL . 'modules/bridge-testimonial/',
			'editor_export'     => true, // Defaults to true and can be omitted.
			'enabled'           => true, // Defaults to true and can be omitted.
			'partial_refresh'   => true,
		));
	}
}


FLBuilder::register_module( 'BridgeTestimonialModule', array(
	'general'       => array( // Tab
		'title'         => __( 'General', 'bb-bridge' ), // Tab title
		'sections'      => array( // Tab Sections
			'general'       => array( // Section
				'fields'        => array( // Section Fields
					'person_image'       => array(
						'type'              => 'photo',
						'label'             => __( 'Person Image', 'bb-bridge' ),
					),
					'person_name'        => array(
						'type'              => 'text',
						'label'             => __( 'Person Name', 'bb-bridge' ),
						'default'           => '',
						'size'              => '30',
					),
					'person_position'    => array(
						'type'              => 'text',
						'label'             => __( 'Person Position', 'bb-bridge' ),
						'default'           => '',
						'size'              => '30',
					),
					'person_company'     => array(
						'type'              => 'text',
						'label'             => __( 'Person Company', 'bb-bridge' ),
						'default'           => '',
						'size'              => '30',
					),
					'person_link'        => array(
						'type'              => 'link',
						'label'             => __( 'Company Link', 'bb-bridge' ),
						'default'           => '',
						'placeholder'       => 'http://www.example.com',
					),
					'testimonial_text'   => array(
						'type'              => 'editor',
						'label'             => __( 'Testimonial Text', 'bb-bridge' ),
						'default'           => 'Testimonial text',
						'rows'              => 10
					),
				)
			)
		)
	),
	'style'         => array(
		'title'         => __( 'Style', 'bb-bridge' ),
		'sections'      => array(
			'image'         => array(
				'title'         => __( 'Image', 'bb-bridge' ),
				'fields'        => array(
					'image'              => array(
						'type'               => 'select',
						'label'              => __( 'Image Style', 'bb-bridge' ),
						'default'            => 'default',
						'options'            => array(
							'default'            => __( 'Default', 'bb-bridge' ),
							'circle'             => __( 'Circle', 'bb-bridge' ),
						),
					),
					'image_position'     => array(
						'type'               => 'select',
						'label'              => __( 'Image Position', 'bb-bridge' ),
						'default'            => 'left',
						'options'            => array(
							'left'               => __( 'Left', 'bb-bridge' ),
							'top'                => __( 'Top', 'bb-bridge' ),
							'right'              => __( 'Right', 'bb-bridge' ),
						),
					)		
				)
			),
			'stars'         => array(
				'title'         => __( 'Stars', 'bb-bridge' ),
				'fields'        => array(
					'testimonial_stars'  => array(
						'type'              => 'select',
						'label'             => __( 'Display Five Stars', 'bb-bridge' ),
						'default'           => 'yes',
						'options'           => array(
							'yes'              => __( 'Yes', 'bb-bridge' ),
							'no'               => __( 'No', 'bb-bridge' ),
						)
					),	
				)
			),
			'alignment'     => array(
				'title'         => __( 'Alignment', 'bb-bridge' ),
				'fields'        => array(
					'alignment'          => array(
						'type'               => 'select',
						'label'              => __( 'Alignment', 'bb-bridge' ),
						'default'            => 'left',
						'options'            => array(
							'left'               => __( 'Left', 'bb-bridge' ),
							'center'             => __( 'Center', 'bb-bridge' ),
							'right'              => __( 'Right', 'bb-bridge' ),
						),
					),
				),
			),
			'background'    => array(
				'title'         => __( 'Background', 'bb-bridge' ),
				'fields'        => array(
					'background'         => array(
						'type'               => 'select',
						'label'              => __( 'Type', 'bb-bridge' ),
						'default'            => 'none',
						'options'            => array(
							'none'              => __( 'None', 'bb-bridge' ),
							'color'             => __( 'Color', 'bb-bridge' ),
						),
						'toggle'             => array(
							'color'              => array(
								'fields'             => array( 'bg_color' ),
							),
						),
					),
					'bg_color'           => array(
						'type'               => 'color',
						'label'              => __( 'Background Color', 'bb-bridge' ),
						'default'            => 'fafafa',
						'show_reset'         => true,
						'show_alpha'         => true,
					),			
				)
			),
			'colors'        => array(
				'title'         => __( 'Colors', 'bb-bridge' ),
				'fields'        => array(
					'color'              => array( 
						'type'               => 'color',
						'label'              => __( 'Text Color', 'bb-bridge' ),
						'default'            => '000000',
						'show_reset'         => true,
					),
					'link_color'         => array( 
						'type'               => 'color',
						'label'              => __( 'Link Color', 'bb-bridge' ),
						'default'            => '000000',
						'show_reset'         => true,
					),
					'link_hover_color'   => array( 
						'type'               => 'color',
						'label'              => __( 'Link Hover Color', 'bb-bridge' ),
						'default'            => '444444',
						'show_reset'         => true,
					),
				),
			),
			'corners'        => array(
				'title'          => __( 'Corner Radius', 'bb-bridge' ),
				'fields'         => array(
					'top_left'               => array(
						'type'                   => 'text',
						'label'                  => __( 'Top Left', 'bb-bridge' ),
						'description'            => 'px',
						'default'                => '',
						'size'                   => '5',
						'placeholder'            => '0'
					),
					'top_right'              => array(
						'type'                   => 'text',
						'label'                  => __( 'Top Right', 'bb-bridge' ),
						'description'            => 'px',
						'default'                => '',
						'size'                   => '5',
						'placeholder'            => '0'
					),
					'bottom_left'            => array(
						'type'                   => 'text',
						'label'                  => __( 'Bottom Left', 'bb-bridge' ),
						'description'            => 'px',
						'default'                => '',
						'size'                   => '5',
						'placeholder'            => '0'
					),
					'bottom_right'           => array(
						'type'                   => 'text',
						'label'                  => __( 'Bottom Right', 'bb-bridge' ),
						'description'            => 'px',
						'default'                => '',
						'size'                   => '5',
						'placeholder'            => '0'
					),
				),
			),
			'border'         => array(
				'title'          => __( 'Border', 'bb-bridge' ),
				'fields'         => array(
					'border_style'       => array(
						'type'               => 'select',
						'label'              => __( 'Type', 'bb-bridge' ),
						'default'            => 'none',
						'options'            => array(
							'none'               => __( 'None', 'bb-bridge' ),
							'solid'              => __( 'Solid', 'bb-bridge' ),
							'dashed'             => __( 'Dashed', 'bb-bridge' ),
							'dotted'             => __( 'Dotted', 'bb-bridge' ),							
							'double'             => __( 'Double', 'bb-bridge' ),
						),
						'toggle'             => array(
							'solid'              => array(
								'fields'             => array( 'border_color', 'border_width' ),
							),
							'dashed'              => array(
								'fields'             => array( 'border_color', 'border_width' ),
							),
							'dotted'              => array(
								'fields'             => array( 'border_color', 'border_width' ),
							),
							'double'              => array(
								'fields'             => array( 'border_color', 'border_width' ),
							),
						),
					),
					'border_color'       => array( 
						'type'               => 'color',
						'label'              => __( 'Color', 'bb-bridge' ),
						'default'            => '',
						'show_reset'         => true,
						'show_alpha'         => true,
					),
					'border_width'       => array( 
						'type'               => 'dimension',
						'label'              => __( 'Width', 'bb-bridge' ),
						'description'        => 'px',
						'default'            => '',
						'placeholder'        => '0',
					),					
				),
			),
			'padding'        => array(
				'title'          => __( 'Padding', 'bb-bridge' ),
				'fields'         => array(
					'padding'           => array(
						'type'              => 'dimension',
						'label'             => __( 'Padding', 'bb-bridge' ),
						'description'       => 'px',
						'placeholder'       => '0',
					),
				),
			),
		),
	),
	'typography'    => array(
		'title'         => __( 'Typography', 'bb-bridge' ),
		'sections'      => array(
			'text_font'     => array(
				'title'         => __( 'Text Font', 'bb-bridge' ),
				'fields'        => array(
					'font_family'       => array(
						'type'              => 'font',
						'label'             => __( 'Font Family', 'bb-bridge' ),
						'default'           => array(
							'family'            => 'Default',
							'weight'            => 'Default'
						),
					),
				)
			)
		)
	),
));